jQuery.expr[':'].contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase()
      .indexOf(m[3].toUpperCase()) >= 0;
};
$('.order-summary__sections').bind('DOMSubtreeModified',function() {
  if ($('div.review-block__content:contains("po box"), div.review-block__content:contains("pobox"), div.review-block__content:contains("p.o box"), div.review-block__content:contains("po. box"), div.review-block__content:contains("p.o.box"), div.review-block__content:contains("p.o. box"), div.review-block__content:contains("p. obox"), div.review-block__content:contains(" ak "), div.review-block__content:contains(" hi ")').length > 0){
    $("div.section > .content-box:first-child").after('<div class="pobox-notification">"PO BOX, Alaska and Hawai" addresses do not qualifty for Expedited and Priority Shipping, learn more <a title="Terms of service" data-modal="policy-31244035" data-close-text="Close" href="/19331869/policies/31244035.html">here</a>.</div>');
    $("input#checkout_shipping_rate_id_shopify-standard20shipping-000").click();
    $('span.radio__label__primary:contains("priority")').closest('.content-box__row').addClass('pobox-disable');
    $('span.radio__label__primary:contains("expedited")').closest('.content-box__row').addClass('pobox-disable');
  }
  if($('div.pobox-notification').length > 1) {
    $('div.pobox-notification').slice(1).remove();
  }
});
